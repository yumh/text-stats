CC=clang
CDEF=-D DEBUG
LDFLAG=
CDEB=-O2 -Wall -ggdb3
CFLAGS=$(CDEB) -D_POSIX_C_SOURCE=200809L
TARGET=text_stats

.PHONY: clean tags doc release strict

all: $(TARGET)

release:
	make CDEB="-O3"

strict:
	make release CDEF="-D STRICT"

$(TARGET): text_stats.o state.o arguments.o strtable.o text_helper.o
	$(CC) $(CFLAGS) $(LDFLAG) $^ -o $@

text_stats.o: state.h arguments.h text_helper.h text_stats.c
	$(CC) $(CFLAGS) $(CDEF) -c text_stats.c

state.o: strtable.h text_helper.h state.h state.c
	$(CC) $(CFLAGS) $(CDEF) -c state.c

arguments.o: state.h arguments.h arguments.c
	$(CC) $(CFLAGS) $(CDEF) -c arguments.c

strtable.o: strtable.h strtable.c
	$(CC) $(CFLAGS) $(CDEF) -c strtable.c

text_helper.o: text_helper.h text_helper.c
	$(CC) $(CFLAGS) $(CDEF) -c text_helper.c

tags:
	rm -f tags
	ctags -R .

doc:
	cd doc && pdflatex text_stats.tex

clean:
	rm -f $(TARGET) *.o tags doc/text_stats.pdf doc/*.{log,aux,out}
