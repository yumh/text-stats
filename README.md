Text Stats
==========

Vedere `doc/text_stats.md` per la documentazione.


## Compilazione

Per compilare:
    
    make

Il compilatore usato di default è `clang` ma si possono usare altri
compilatori, ad esempio `gcc`

    make CC=gcc


## Build di debug

È possibile compilare delle build con delle funzionalità di debug

    make debug

Queste build includono un'ulteriore opzione da linea di comando `-s` simile a
`-i` o `-I` ma che inibisce la visualizzazione dei report. Sono presenti anche
delle informazioni extra (stampate nello `stderr`).


# Ctags

Per creare velocemente un file `tag` per navigare il progetto:

    make tags


# Uso

    ./text_stats

Si consulti la documentazione per ulteriori informazioni.
