#include "arguments.h"

// Legge gli argomenti e altera lo stato
void parg(int argc, char **argv, state_t *state) {

    int flag;

    while ((flag = getopt(argc, argv, ARGUMENTS_FLAG)) != -1) {
        switch (flag) {
            case 'w':
                state->fl_wd = 1;
                state_push_arg(state, WORDS);
                break;
            case 'l':
                state->fl_ln = 1;
                state_push_arg(state, LINES);
                break;
            case 'c':
                state->fl_ch = 1;
                state_push_arg(state, CHARS);
                break;
            case 'i':
                state->fl_hi = 1;
                state_push_arg(state, HISTS);
                break;
            case 'I':
                state->fl_hi = 2;
                state_push_arg(state, HISTS);
                break;

#ifdef DEBUG
            case 's':
                state->fl_hi = 3;
                state_push_arg(state, HISTS);
                break;
#endif

            case 'h':
                state->fl_us = 1;
                break;
            default:
                state->fl_us = 1;
                state->exit_status = EX_USAGE;
                break;
        }
    }

    // Devo leggere dallo stdin o da un file?
    if (optind < argc) {
        state->in = fopen(argv[optind], "r");
        state->in_is_stdin = 0;

        // Il file si può davvero aprire?
        if (state->in == NULL) {
            // non ha senso "chiudere" un file che non esiste
            state->in_is_stdin = 1;
            fprintf(stderr, "%s: unable to open \"%s\": %s\n", argv[0], argv[optind], strerror(errno));
            state->exit_status = EX_NOINPUT;
        }
    }

    // Nessuna opzione settata? Le setto tutte!
    if (state->fl_ch + state->fl_hi + state->fl_ln + state->fl_wd == 0) {
        state->fl_ch = 1;
        state->fl_hi = 1;
        state->fl_ln = 1;
        state->fl_wd = 1;

        state_push_arg(state, CHARS);
        state_push_arg(state, WORDS);
        state_push_arg(state, LINES);
        state_push_arg(state, HISTS);
    }
}
