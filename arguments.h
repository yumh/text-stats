#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "state.h"

extern char *optarg;
extern int optind;

// Legge gli argomenti da `argc` e `argv` e muta lo `state` di conseguenza.
void parg(int argc, char **argv, state_t* state);
