Text Stats
==========

Omar Polo -- 127782


Cos'è "text stats"
------------------

"text stats" è una utility da linea di comando scritta in C che si occupa di
calcolare delle statistiche sul testo inserito. Queste statistiche si
riferiscono al numero di parole/caratteri/linee nel documento e anche il numero
di occorrenze delle singole parole nel documento inserito.


Uso
---

    ./text_stats

Senza nessuna opzione inizierà a leggere dallo `stdin`. Alla fine presenterà un
report con il totale di parole/caratteri/linee inserite e una lista di parole e
relative occorrenze. Ad esempio:

    ./text_stats
    Ciao come va?
    Bene, grazie, e tu?

    Conteggio caratteri: 34
    Conteggio linee: 2
    Conteggio parole: 7
     - "e": 1
     - "tu": 1
     - "va": 1
     - "Ciao": 1
     - "come": 1
     - "grazie": 1
     - "bene": 1

Senza nessuna opzione abilitata esplicitamente verranno abilitati tutti i
report.

TODO: discussione sugli argomenti

TODO: `make debug`


Implementazione
---------------

Come prima cosa viene effettuato il parsing degli argomenti da linea di comando.
Per semplificare questa operazione è presente una `struct state_t`
(`state.h:12`) che contiene alcune variabili. Ciò semplifica il passaggio di
dati tra la funzione `main` e `parg`.

Successivamente il programma inizia a leggere caratteri dal file (`stdin` oppure
un file specificato) fino a consumare il file. Per ogni carattere vengono
incrementate le dovute statistiche. Ad esempio, se il carattere è un ****line
feed* (`\n`) verrà incrementato il numero totale dei caratteri e il numero
totale delle linee.

Man mano che l'input procede il programma cerca di "scrivere" la parola corrente
in uno spazio di memoria allocato dinamicamente. Quando una parola termina viene
incrementato il contatore delle parole e, se richiesto dall'utente, la parola
corrente verrà aggiunta in una hash table.

Per tenere traccia delle parole e delle relative occorrenze, è stata
implementata una tabella di hashing. In questa implementazione, la tabella di
hashing è un array di linked list. Le collisioni vengono gestite, appunto,
grazie alle linked list. La funzione di hashing (`strtable.h:26`) ritorna un
`unsigned int` che verrà usato come indice dell'array.


Test
----

Il programma è stato testato completamente su Arch Linux (kernel 4.8.7-1-ARCH)
sia con clang (3.9.0) che con gcc (6.2.1).

Nel tarball con i sorgeti dovrebbe essere presente anche una cartella `tests/`
contenente dei file di testo che sono stati usati come prova.

Vorrei sottolineare come, sulla mia macchina, `text_stats` per effettuare il
parsing de "I promessi sposi" (`tests/promessi-sposi.txt`) impieghi 0.870s per
il report completo, mentre 0.040s per i report sul numero di
parole/linee/caratteri.

Per quanto riguarda la funzione di hashing usata, per quanto migliorabile, credo
che la versine attuale sia, tutto sommato, soddifacente. Sempre nel caso dei
promessi sposi, con ben 20941 indici creati si hanno solo 2006 collisioni.
