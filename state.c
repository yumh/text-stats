#include "state.h"

state_t *create_state() {
    state_t *s = malloc(sizeof(state_t));
    if (!s) {
        exit(EXIT_FAILURE);
    }

    // di default assumiamo `stdin` come fonte per l'input
    s->in_is_stdin = 1;
    s->in = stdin;

    s->fl_ln = 0;
    s->fl_wd = 0;
    s->fl_ch = 0;
    s->fl_hi = 0;

    // inizializzo l'ordine a NONE
    for (int i = 0; i < STATE_ORDER_LENGTH; ++i)
        s->order[i] = NONE;

    s->fl_us = 0;
    s->exit_status = EX_OK;

    return s;
}

void destroy_state(state_t *s) {
    // in caso chiudi il file
    if (!s->in_is_stdin) {
        fclose(s->in);
    }

    free(s);
}

void state_push_arg(state_t *state, arg_t a) {
    if (a == NONE)
        return;

    for (int i = 0; i < STATE_ORDER_LENGTH; ++i) {
        if (state->order[i] == a)
            return;

        if (state->order[i] == NONE) {
            state->order[i] = a;
            return;
        }
    }
}

char *state_argtype_string(arg_t a) {
    switch (a) {
        case NONE:
            return "none";
        case LINES:
            return "lines";
        case WORDS:
            return "words";
        case CHARS:
            return "chars";
        case HISTS:
            return "histogram";
    }

    return "(?)";
}
