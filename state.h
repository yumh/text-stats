#ifndef STATE_H
#define STATE_H

#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

#include "strtable.h"
#include "text_helper.h"

#define STATE_ORDER_LENGTH 4

typedef enum {
    NONE,
    LINES,
    WORDS,
    CHARS,
    HISTS,
} arg_t;

// Dichiarazione del tipo "stato"
typedef struct {
    // Gestione dell'input
    int in_is_stdin;
    FILE *in;

    // I flag richiesti dall'utente
    int fl_ln;
    int fl_wd;
    int fl_ch;
    int fl_hi;
    int fl_us;

    // l'ordine con il quel gli argomenti sono stati passati
    arg_t order[STATE_ORDER_LENGTH];

    // il codice di uscita
    int exit_status;
} state_t;

// Costruttore: crea lo stato con dei valori di default
state_t *create_state();

// Distrugge lo stato, chiamando le opportune funzioni "a cascata" (come
// `destroy_state` per la tabella di hash)
void destroy_state(state_t*);

// l'ordine viene visto come uno stack. Questa funzione aggiunge in coda il tipo
// di argomento desiderato
void state_push_arg(state_t *state, arg_t);

char *state_argtype_string(arg_t a);

#endif
