#include "strtable.h"

// Bernstein's hash (modificato)
unsigned long hash(const char *a, int modulo) {
    unsigned long h = 7;
    for (; *a; ++a)
        h = ((h << 5) + h) + *a;
    return h % modulo;
}

table_t *create_table() {
    table_t *t = malloc(sizeof(table_t));
    if (!t) {
        exit(EXIT_FAILURE);
    }

    t->size = TABLE_MIN_SIZE;
    t->loaded = 0;
    t->items = calloc(TABLE_MIN_SIZE, sizeof(cell*));
    if (!t->items) {
        exit(EXIT_FAILURE);
    }

    return t;
}

float table_load_factor(table_t *t) {
    return (float) t->loaded/t->size;
}

unsigned long in_table_words(table_t *t) {
    return t->loaded;
}

void destroy_table(table_t *t) {

    for (unsigned long i = 0; i < t->size; ++i)
        if (t->items[i] != NULL)
            destroy_cell(t->items[i]);

    free(t->items);

    free(t);
}

cell *create_empty_cell() {
    cell *c = malloc(sizeof(cell));
    if (!c) {
        exit(EXIT_FAILURE);
    }
    c->word = NULL;
    c->occurrences = 0;
    return c;
}

void destroy_cell(cell *c) {
    free(c->word);
    free(c);
}

unsigned long find_index_for(table_t *t, const char *key) {
    unsigned long index = hash(key, t->size);

    while (t->items[index] != NULL && strcmp(t->items[index]->word, key) != 0)
        index = (index+1) % t->size;

    return index;
}

void fc_inc(table_t *t, const char *key) {

    // Allarga la tabella se necessario
    if (table_load_factor(t) >= TABLE_MAX_LOAD_FACTOR ) {

        int old_size = t->size;
        cell **old_items = t->items;

        t->size *= 2;
        t->items = calloc(t->size, sizeof(cell*));
        if (!t->items) {
            exit(EXIT_FAILURE);
        }

        for (unsigned long i = 0; i < old_size; ++i) {
            cell *cc = old_items[i];
            if (cc == NULL) continue;
            unsigned long new_index = find_index_for(t, cc->word);
            t->items[new_index] = cc;
        }

        free(old_items);
    }

    unsigned long index = find_index_for(t, key);

    if (t->items[index] == NULL) {
        t->items[index] = create_empty_cell();
        t->items[index]->word = strdup(key);

        t->loaded++;
    }

    t->items[index]->occurrences += 1;
}

void dotable(table_t *t, void(*f)(const char*,int)) {
    for (unsigned long i = 0; i < t->size; ++i)
        if (t->items[i] != NULL)
            f(t->items[i]->word, t->items[i]->occurrences);
}
