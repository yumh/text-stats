#ifndef STRTABLE_H
#define STRTABLE_H

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define TABLE_MIN_SIZE 64
#define TABLE_MAX_LOAD_FACTOR 0.70
#define FANCY_PRIME_NUMBER 113

typedef struct {
    char *word;
    int occurrences;
} cell;

// Una hash table
typedef struct {
    unsigned long size;
    unsigned long loaded;
    cell **items;
} table_t;

// Un algoritmo di hashing basato sulla funzione di hash di Bernstein
unsigned long hash(const char*, int modulo);

// Inizializza una nuova tabella.
table_t *create_table();

float table_load_factor(table_t*);

// Calcola il numero totale di parole inserite nella tabella di hash
unsigned long int in_table_words(table_t*);

// Libera la memoria allocata dalla tabella
void destroy_table(table_t*);

// crea una cella con parola `char*` e occorrenze 0
cell *create_empty_cell();

// Distrugge una cella liberandone lo spazio allocato
void destroy_cell(cell*);

unsigned long find_index_for(table_t*, const char*);

// Trova e incrementa (oppure aggiunge) una parola alla tabella di hash
void fc_inc(table_t*, const char*);

// Applica una funzione per ogni parola inserita nella tabella di hash
void dotable(table_t*, void(*)(const char*,int));

#endif
