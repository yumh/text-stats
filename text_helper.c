#include "text_helper.h"

void usage(const char *prgname) {
    printf("Uso:\t%s [-%s] [file]\n", prgname, ARGUMENTS_FLAG);
    printf("  -w   per contare le parole\n");
    printf("  -l   per contare le linee\n");
    printf("  -c   per contare i caratteri\n");
    printf("  -i   per mostrare un elenco di parole e relative occorrenze\n");
    printf("  -I   simile a -i ma stampa un istogramma\n");

#ifdef DEBUG
    printf("  -s   genera le statistiche sulle parole ma non le stampa [DEBUG]\n");
#endif

    printf("  -h   stampa questo aiuto\n");
    putchar('\n');
    printf("Se `file` non è specificato verrà letto lo `stdin`.\n");
}

int is_word_separator(const char c) {
    return strchr(WORD_DELIMITERS, c) != NULL;
}

void print_stat(const char *word, int occ) {
    printf(" - \"%s\": %d\n", word, occ);
}

void print_hist(const char *word, int occ) {
    putchar('|');
    for (; occ > 0; --occ) {
        putchar('-');
    }
    printf(" \"%s\"\n", word);
}
