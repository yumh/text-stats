#ifndef TEXT_HELPER_H
#define TEXT_HELPER_H

#include <stdio.h>
#include <string.h>

#if defined DEBUG
# define ARGUMENTS_FLAG "hwlciIs"
#else
# define ARGUMENTS_FLAG "hwlciI"
#endif


// le parole della lingua italiana sono composte, in media, da 5/6 caratteri.
// pre-allocare 10 caratteri è una buona misura per evitare di dovre reallocare
// troppo spesso lo spazio per la parola corrente
//
// fonte: http://www.proz.com/forum/italian/13480-lunghezza_media_parole_italiane.html
#define WORD_MIN_ALLOC_SIZE 10

// Una serie di caratteri che vengono considerati come separatori di parole
#ifdef STRICT
# define WORD_DELIMITERS " \n\t"
#else
# define WORD_DELIMITERS " \n\r\t<>/\\\"'-+*,.;:?!=()[]{}"
#endif

// Stampa un messaggio all'utente per mostrare il funzionamento del programma.
// prgname è una stringa contente il nome del programma (normalmente argv[0]).
void usage(const char *prgname);

// Il carattere è un separatore di parole? -- funzione "booleana"
int is_word_separator(const char);

// Data una parola e un numero di occorrenze, stampa nello `stdout` la parola e
// le sue occorrenze
void print_stat(const char*, int);

// Data una parola e un numero di occorrenze, stampa nello `stdout` un
// istogramma che rappresenta le sue occorrenze
void print_hist(const char*, int);

#endif
