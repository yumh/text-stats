#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <signal.h>
#include <unistd.h>

#include "state.h"
#include "arguments.h"
#include "text_helper.h"
#include "strtable.h"

state_t *state = NULL;

void handle_sig(int);

int main(int argc, char *argv[])
{
    // handle signal
    signal(SIGTERM, &handle_sig);
    signal(SIGINT, &handle_sig);

    // Definizione di alcune variabili
    char c;

    char *cwrd = malloc(WORD_MIN_ALLOC_SIZE * sizeof(char));
    int cwrd_len = 0;
    int cwrd_mlen = WORD_MIN_ALLOC_SIZE;

    table_t *words = create_table();
    unsigned long ln_n = 0;
    unsigned long wd_n = 0;
    unsigned long ch_n = 0;

    int in_word = 0;
    int exit_status = EXIT_SUCCESS;
    state = create_state();

#ifdef DEBUG
    int total_wrd_res = 0;
#endif

    // Gestione degli argomenti
    parg(argc, argv, state);

    // Esci se si sono verificati errori o se l'utente ha richiesto di stampare
    // l'utilizzo
    if (state->fl_us || state->exit_status) {
        if (state->fl_us) usage(argv[0]);

        exit_status = state->exit_status;

        // Senza dimenticarci però di pulire le risorse
        free(cwrd);
        destroy_table(words);
        destroy_state(state);

        return exit_status;
    }

    // Processiamo il file. Che sia lo `stdin` o un altro file, poco importa
    while ((c = fgetc(state->in)) != EOF && !state->exit_status) {
        ch_n++;

        if (c == '\n') ln_n++;

        if (in_word && is_word_separator(c)) {
            wd_n++;
            in_word = 0;

            if (state->fl_hi) {
                cwrd[cwrd_len] = '\0';
                cwrd_len = 0;
                fc_inc(words, cwrd);
            }
        }

        if (!in_word && !is_word_separator(c)) {
            in_word = 1;
            if (state->fl_hi) cwrd_len = 0;
        }

        if (in_word && state->fl_hi) {
            cwrd[cwrd_len] = c;
            cwrd_len++;
        }

        if (state->fl_hi && cwrd_len == cwrd_mlen) {
            cwrd_mlen *= 2;
            cwrd = realloc(cwrd, cwrd_mlen * sizeof(char));

#ifdef DEBUG
            total_wrd_res++;
#endif

        }
    }

    // stampa i risultati
    for (int i = 0; i < STATE_ORDER_LENGTH; ++i) {
        switch (state->order[i]) {
            case CHARS:
                printf("Conteggio caratteri: %lu\n", ch_n);
                break;
            case LINES:
                printf("Conteggio linee: %lu\n", ln_n);
                break;
            case WORDS:
                printf("Conteggio parole: %lu\n", wd_n);
                break;
            case HISTS:
                if (state->fl_hi == 1) dotable(words, &print_stat);
                if (state->fl_hi == 2) dotable(words, &print_hist);
                break;
            case NONE:
                break;
            default:
#ifdef DEBUG
                fprintf(stderr, "Something strange is happening...\n");
#endif
                break;
        }
    }

    exit_status = state->exit_status;

    // De-alloca tutto
    destroy_table(words);
    free(cwrd);
    destroy_state(state);

    return exit_status;
}

void handle_sig(int signum) {
    if (state) {
        fprintf(stderr, "Exiting...\n");
        state->exit_status = EXIT_FAILURE;
    }
}
