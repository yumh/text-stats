
- togliere alcune costanti/include inutili
- rinominare tutte le occorrenze di `cell` in `cell_t`
- modificare la struttura di tutti gli header. Voglio gli elementi in questo ordine
    1. header guards
    2. define
    3. include (first system then mine)
    3. typedef/enum
    4. functions
